import { CustomerEntity } from '@app/entities/customer.entity';

export const makeCustomerEntityMock = (id?: string): CustomerEntity => {
    const customer = new CustomerEntity();

    customer.id = id;
    customer.name = 'customer_name';

    return customer;
}

export const makeEmptyCustomerEntityMock = (): CustomerEntity => {
    const customer = new CustomerEntity();

    return customer;
}
