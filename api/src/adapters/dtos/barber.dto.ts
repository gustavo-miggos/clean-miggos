import { BarberEntity } from '@app/entities/barber.entity';

export class CreateBarberInputDTO {
    fullName: string;

    map(input: CreateBarberInputDTO): BarberEntity {
        const barber = new BarberEntity();
        barber.name = input.fullName;

        return barber;
    }
}

export type GetBarberInputDTO = { barberId: string };

export type GetBarberOutputDTO = BarberEntity;
