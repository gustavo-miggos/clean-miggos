import { IBarberRepository } from '@app/contracts/barber.repository';
import { BarberEntity } from '@app/entities/barber.entity';
import { barberNotFound } from '@app/errors/barber.error';

export class GetBarberUsecase {

    constructor(
        private barberRepository: IBarberRepository
    ) {}
    
    async handle(id: string): Promise<BarberEntity> {
        const barber = await this.barberRepository.get(id);

        if (!barber) throw barberNotFound();
        
        return barber;
    }
}
