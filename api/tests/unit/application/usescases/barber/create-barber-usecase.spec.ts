import { barberNameMustBeInformed } from '@app/errors/barber.error';
import { makeBarberEntityMock, makeEmptyBarberEntityMock } from '@tests/mocks/application/entities/barber-entity.mock';
import { makeCreateBarberUseCase } from '@tests/mocks/application/usecases/create-barber-usecase.mock';

afterEach(() => jest.clearAllMocks());

describe('Create Barber Use Case', () => {
    it('(handle): should create a barber and return nothing', async () => {
        const usecase = makeCreateBarberUseCase();
        
        const spy = jest.spyOn(usecase, 'handle');
        const response = await usecase.handle(makeBarberEntityMock());

        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(makeBarberEntityMock());
        expect(response).toBe(undefined);
    });

    it('(handle): should throw an error when name is empty', async () => {
        const usecase = makeCreateBarberUseCase();

        const response = usecase.handle(makeEmptyBarberEntityMock());

        await expect(response).rejects.toThrowError(barberNameMustBeInformed());
    });
});
