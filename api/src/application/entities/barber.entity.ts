import { barberNameMustBeInformed } from "@app/errors/barber.error";

export class BarberEntity {
    id?: string;
    name: string;

    constructor(barber?: BarberEntity) {
        Object.assign(this, barber);
        
        if (barber) this.validate();
    }

    private validate(): Error | void {
        if (!this.name) throw barberNameMustBeInformed();
    }
}
