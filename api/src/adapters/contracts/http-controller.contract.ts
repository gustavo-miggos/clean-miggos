import { IHttpRequest } from '@adapters/contracts/http-request.contract';
import { IHttpResponse } from '@adapters/contracts/http-response.contract';

export interface IHttpController {
    handle(request: IHttpRequest): Promise<IHttpResponse<any>>;
}
