import { IBarberRepository } from '@app/contracts/barber.repository';
import { BarberEntity } from '@app/entities/barber.entity';
import { makeBarberEntityMock } from '@tests/mocks/application/entities/barber-entity.mock';

export class InMemoryBarberRepository implements IBarberRepository {

    create(barber: BarberEntity): Promise<void> {
        return Promise.resolve();
    }

    get(id: string): Promise<BarberEntity> {
        const barber = makeBarberEntityMock();

        barber.id = id;

        return Promise.resolve(barber);
    }

}