import { IScheduleRepository } from '@app/contracts/schedule.repository';
import { ScheduleEntity } from '@app/entities/schedule.entity';
import { appointmentBeforeThanToday, invalidAppointmentDate } from '@app/errors/schedule.error';

export class ScheduleHaircutUsecase {

    constructor(
        private scheduleRepository: IScheduleRepository.Contract,
    ) { }

    async handle(schedule: ScheduleEntity): Promise<ScheduleEntity> {
        const scheduleInstance = new ScheduleEntity(schedule);

        if (!scheduleInstance.isAppointmentDateValid()) {
            throw invalidAppointmentDate();
        }

        if (scheduleInstance.isAppointmentBeforeThanToday()) {
            throw appointmentBeforeThanToday();
        }

        return this.scheduleRepository.schedule({ ...scheduleInstance, date: schedule.appointment });
    }
}
