import { customerNameMustBeInformed } from "@app/errors/customer.error";

export class CustomerEntity {
    id?: string;
    name: string;

    constructor(customer?: CustomerEntity) {
        Object.assign(this, customer);

        if (customer) this.validate();
    }

    private validate(): Error | void {
        if (!this.name) throw customerNameMustBeInformed();
    }
}
