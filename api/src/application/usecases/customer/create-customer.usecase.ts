import { ICustomerRepository } from '@app/contracts/customer.repository';
import { CustomerEntity } from '@app/entities/customer.entity';

export class CreateCustomerUsecase {
    constructor(
        private customerRepository: ICustomerRepository 
    ) {}

    async handle(customer: CustomerEntity): Promise<void> {
        const customerInstance = new CustomerEntity(customer);

        return this.customerRepository.create(customerInstance);
    }
}
