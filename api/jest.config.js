/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */

module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'node',
  testMatch: [
    "**/unit/**/*.spec.ts",
  ],
  moduleNameMapper: {
    '@app/(.*)': '<rootDir>/src/application/$1',
    '@adapters/(.*)': '<rootDir>/src/adapters/$1',
    '@main/(.*)': '<rootDir>/src/main/$1',
    '@tests/(.*)': '<rootDir>/tests/$1',
  },
};
