import { BarberEntity } from '@app/entities/barber.entity';
import { CustomerEntity } from '@app/entities/customer.entity';
import { appointmentBeforeThanToday, invalidAppointmentDate } from '@app/errors/schedule.error';

export class ScheduleEntity {
    barber: BarberEntity;
    customer: CustomerEntity;
    appointment: Date;

    constructor(schedule?: ScheduleEntity) {
        Object.assign(this, schedule);

        if (schedule) this.validate();
    }

    private validate(): Error | void {
        if (!this.isAppointmentDateValid()) throw invalidAppointmentDate();

        if(this.isAppointmentBeforeThanToday()) throw appointmentBeforeThanToday();
    }

    isAppointmentDateValid(): boolean {
        return this.appointment instanceof Date && !isNaN(this.appointment.getTime());
    }

    isAppointmentBeforeThanToday(): boolean {
        const today = new Date();
        
        return this.appointment < today;
    }
}
