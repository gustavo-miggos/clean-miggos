import { BarberEntity } from '@app/entities/barber.entity';
import { CustomerEntity } from '@app/entities/customer.entity';
import { ScheduleEntity } from '@app/entities/schedule.entity';

export namespace IScheduleRepository {

    export type ScheduleParams = {
        date: Date;
        barber: BarberEntity;
        customer: CustomerEntity;
    }

    export interface Contract {
        schedule(schedule: ScheduleParams): Promise<ScheduleEntity>;
    }
}
