import { BarberEntity } from "@app/entities/barber.entity";

export interface IBarberRepository {

    create(barber: BarberEntity): Promise<void>;
    get(id: string): Promise<BarberEntity>;
}
