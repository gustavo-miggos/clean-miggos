import { CustomerEntity } from '@app/entities/customer.entity';

export class CreateCustomerInputDTO {
    fullName: string;

    map(input: CreateCustomerInputDTO): CustomerEntity {
        const customer = new CustomerEntity();
        customer.name = input.fullName;

        return customer;
    }
}

export type GetCustomerInputDTO = { customerId: string };

export type GetCustomerOutputDTO = CustomerEntity;
