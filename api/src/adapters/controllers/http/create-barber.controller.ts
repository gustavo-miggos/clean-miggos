import { IHttpController } from '@adapters/contracts/http-controller.contract';
import { IHttpRequest } from '@adapters/contracts/http-request.contract';
import { IHttpResponse } from '@adapters/contracts/http-response.contract';
import { CreateBarberInputDTO } from '@adapters/dtos/barber.dto';
import { created, serverError } from '@adapters/helpers/http-codes.helper';
import { CreateBarberUsecase } from '@app/usecases/barber/create-barber.usecase';

export class CreateBarberHttpController implements IHttpController {
    
    constructor(
        private createBarberUsecase: CreateBarberUsecase
    ) {}
    
    async handle(request: IHttpRequest<CreateBarberInputDTO>): Promise<IHttpResponse<void>> {
        try {
            const input = new CreateBarberInputDTO();

            await this.createBarberUsecase.handle(input.map(request.body));

            return created();
        } catch(err) {
            return serverError(err as Error);
        }
    }

}
