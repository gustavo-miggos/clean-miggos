import { CreateBarberUsecase } from '@app/usecases/barber/create-barber.usecase';
import { InMemoryBarberRepository } from '@tests/mocks/adapters/repositories/db/in-memory-barber-repository.mock';

export const makeCreateBarberUseCase = (): CreateBarberUsecase => {
    return new CreateBarberUsecase(
        new InMemoryBarberRepository()
    );
};
