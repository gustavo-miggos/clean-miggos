
import { IHttpResponse } from '@adapters/contracts/http-response.contract';

export const badRequest = (message?: string): IHttpResponse => ({
  statusCode: 400,
  body: { message },
});

export const forbidden = (error: Error): IHttpResponse => ({
  statusCode: 403,
  body: { message: error.message },
});

export const unauthorized = (): IHttpResponse => ({
  statusCode: 401,
  body: null,
});

export const serverError = (error: Error): IHttpResponse => {
  return {
    statusCode: 500,
    body: { message: 'Unexpected error' },
  }
};

export const ok = (body: any): IHttpResponse => ({
  statusCode: 200,
  body: body,
});

export const created = (): IHttpResponse => ({
  statusCode: 201,
  body: null,
});

export const noContent = (): IHttpResponse => ({
  statusCode: 204,
  body: null,
});
