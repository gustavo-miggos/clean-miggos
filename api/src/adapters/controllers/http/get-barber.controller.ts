import { IHttpController } from '@adapters/contracts/http-controller.contract';
import { IHttpRequest } from '@adapters/contracts/http-request.contract';
import { IHttpResponse } from '@adapters/contracts/http-response.contract';
import { GetBarberInputDTO, GetBarberOutputDTO } from '@adapters/dtos/barber.dto';
import { ok, serverError } from '@adapters/helpers/http-codes.helper';
import { GetBarberUsecase } from '@app/usecases/barber/get-barber.usecase';

export class GetBarberHttpController implements IHttpController {

    constructor(
        private getBarberUsecase: GetBarberUsecase
    ) {}

    async handle(request: IHttpRequest<null, GetBarberInputDTO>): Promise<IHttpResponse<GetBarberOutputDTO>> {

        try {
            const response = await this.getBarberUsecase.handle(request.params.barberId);

            return ok(response);
        } catch(err) {
            return serverError(err as Error);
        }
    }

}
