import { IBarberRepository } from "@app/contracts/barber.repository";
import { BarberEntity } from "@app/entities/barber.entity";

export class CreateBarberUsecase {
    
    constructor(
        private barberRepository: IBarberRepository
    ) {}
    
    async handle(barber: BarberEntity): Promise<void> {
        const barberInstance = new BarberEntity(barber);

        return this.barberRepository.create(barberInstance);
    }
}
