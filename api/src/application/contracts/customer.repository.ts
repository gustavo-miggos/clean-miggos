import { CustomerEntity } from '@app/entities/customer.entity';

export interface ICustomerRepository {
    create(customer: CustomerEntity): Promise<void>;
    get(id: string): Promise<CustomerEntity>;
}
