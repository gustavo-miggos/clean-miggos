import { ICustomerRepository } from '@app/contracts/customer.repository';
import { CustomerEntity } from '@app/entities/customer.entity';
import { customerNotFound } from '@app/errors/customer.error';

export class GetCustomerUsecase {
    constructor(
        private customerRepository: ICustomerRepository 
    ) {}

    async handle(id: string): Promise<CustomerEntity> {
        const customer = await this.customerRepository.get(id);

        if (!customer) throw customerNotFound();

        return customer;
    }
}
