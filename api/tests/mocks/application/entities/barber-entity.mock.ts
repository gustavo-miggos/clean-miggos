import { BarberEntity } from '@app/entities/barber.entity';

export const makeBarberEntityMock = (id?: string): BarberEntity => {
    const barber = new BarberEntity();

    barber.id = id;
    barber.name = 'Barber_name';

    return barber;
}

export const makeEmptyBarberEntityMock = (): BarberEntity => {
    const barber = new BarberEntity();

    return barber;
}
