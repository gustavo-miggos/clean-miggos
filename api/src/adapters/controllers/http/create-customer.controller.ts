import { IHttpController } from '@adapters/contracts/http-controller.contract';
import { IHttpRequest } from '@adapters/contracts/http-request.contract';
import { IHttpResponse } from '@adapters/contracts/http-response.contract';
import { CreateCustomerInputDTO } from '@adapters/dtos/customer.dto';
import { created, serverError } from '@adapters/helpers/http-codes.helper';
import { CreateCustomerUsecase } from '@app/usecases/customer/create-customer.usecase';

export class CreateCustomerHttpController implements IHttpController {
    
    constructor(
        private createCustomerUsecase: CreateCustomerUsecase
    ) {}
    
    async handle(request: IHttpRequest<CreateCustomerInputDTO>): Promise<IHttpResponse<void>> {
        try {
            const input = new CreateCustomerInputDTO();

            await this.createCustomerUsecase.handle(input.map(request.body));

            return created();
        } catch(err) {
            return serverError(err as Error);
        }
    }

}
