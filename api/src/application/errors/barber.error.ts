export const barberNameMustBeInformed = () => new Error('Barber name must be informed');

export const barberNotFound = () => new Error('Barber not found');
