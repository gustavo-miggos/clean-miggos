import { customerNameMustBeInformed } from '@app/errors/customer.error';
import { makeCustomerEntityMock, makeEmptyCustomerEntityMock } from '@tests/mocks/application/entities/customer-entity.mock';
import { makeCreateCustomerUseCase } from '@tests/mocks/application/usecases/create-customer-usecase.mock';

afterEach(() => jest.clearAllMocks());

describe('Create Customer Use Case', () => {
    it('(handle): should create a customer and return nothing', async () => {
        const usecase = makeCreateCustomerUseCase();
        
        const spy = jest.spyOn(usecase, 'handle');
        const response = await usecase.handle(makeCustomerEntityMock());

        expect(spy).toHaveBeenCalledTimes(1);
        expect(spy).toHaveBeenCalledWith(makeCustomerEntityMock());
        expect(response).toBe(undefined);
    });

    it('(handle): should throw an error when name is empty', async () => {
        const usecase = makeCreateCustomerUseCase();

        const response = usecase.handle(makeEmptyCustomerEntityMock());

        await expect(response).rejects.toThrowError(customerNameMustBeInformed());
    });
});
