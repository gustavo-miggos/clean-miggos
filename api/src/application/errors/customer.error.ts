export const customerNameMustBeInformed = () => new Error('Customer name must be informed');

export const customerNotFound = () => new Error('Customer not found');
