import { CreateCustomerUsecase } from '@app/usecases/customer/create-customer.usecase';
import { InMemoryCustomerRepository } from '@tests/mocks/adapters/repositories/db/in-memory-customer-repository.mock';

export const makeCreateCustomerUseCase = (): CreateCustomerUsecase => {
    return new CreateCustomerUsecase(
        new InMemoryCustomerRepository()
    );
};
