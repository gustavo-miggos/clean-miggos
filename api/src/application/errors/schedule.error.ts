export const invalidAppointmentDate = () => new Error('Invalid appointment date');

export const appointmentBeforeThanToday = () => new Error(`Appointment date can't be before than today`);
