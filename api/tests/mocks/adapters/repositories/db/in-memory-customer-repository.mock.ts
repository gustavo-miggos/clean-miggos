import { ICustomerRepository } from '@app/contracts/customer.repository';
import { CustomerEntity } from '@app/entities/customer.entity';
import { makeCustomerEntityMock } from '@tests/mocks/application/entities/customer-entity.mock';

export class InMemoryCustomerRepository implements ICustomerRepository {

    create(customer: CustomerEntity): Promise<void> {
        return Promise.resolve();
    }

    get(id: string): Promise<CustomerEntity> {
        const customer = makeCustomerEntityMock();

        customer.id = id;

        return Promise.resolve(customer);
    }

}