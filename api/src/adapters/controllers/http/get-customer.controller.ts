import { IHttpController } from '@adapters/contracts/http-controller.contract';
import { IHttpRequest } from '@adapters/contracts/http-request.contract';
import { IHttpResponse } from '@adapters/contracts/http-response.contract';
import { GetCustomerInputDTO, GetCustomerOutputDTO } from '@adapters/dtos/customer.dto';
import { ok, serverError } from '@adapters/helpers/http-codes.helper';
import { GetCustomerUsecase } from '@app/usecases/customer/get-barber.usecase';

export class GetCustomerHttpController implements IHttpController {

    constructor(
        private getCustomerUsecase: GetCustomerUsecase
    ) {}

    async handle(request: IHttpRequest<null, GetCustomerInputDTO>): Promise<IHttpResponse<GetCustomerOutputDTO>> {

        try {
            const response = await this.getCustomerUsecase.handle(request.params.customerId);

            return ok(response);
        } catch(err) {
            return serverError(err as Error);
        }
    }

}
